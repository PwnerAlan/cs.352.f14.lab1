package utils

import (
	"os"
	"path"
)

// FindAllPairsExecutable returns the path to the allpairs application.
func FindAllPairsExecutable() string {
	paths := []string{
		"./allpairs",
		"../allpairs",
		path.Join(os.Getenv("GOPATH"), "bin", "allpairs"),
		path.Join(os.Getenv("GOPATH"), "src", "allpairs", "allpairs"),
	}

	if path.Base(os.Args[0]) == "allpairs" {
		return os.Args[0]
	}

	for _, path := range paths {
		if stat, err := os.Stat(path); err == nil && stat.Mode().IsRegular() {
			return path
		}
	}

	return ""
}
