package utils

// Pair of  values.
type Pair struct {
	First, Second string
}

// Generate combinations of pairs from given set of strings.
func GenerateCombinations(strings []string, all bool) <-chan Pair {
	return nil
}

const DefaultChunkSize = 8 // Default Chunk Size

// Generate chunks of fixed-size from input slice of strings.
func GenerateChunks(strings []string, size int) <-chan []string {
	return nil
}
